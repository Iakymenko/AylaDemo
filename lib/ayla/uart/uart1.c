/*
 * USART 1 lib
*/
#include <stdlib.h>
//#include <ayla/mcu_platform.h>
#include <stm32f30x_rcc.h>
#include <stm32f30x_gpio.h>
#include "stm32f30x_usart.h"
//#include <mcu_io.h>

#include "ayla/uart1.h"
#include "stm32.h"





/*****************************************************
 * Initialize USART1: enable interrupt on reception
 * of a character
 *****************************************************/
void USART1_Init(void)
{
//	uint32_t apbclock;
//	uint32_t br;
//	RCC_ClocksTypeDef RCC_ClocksStatus;

//	/* USART configuration structure for USART1 */
	USART_InitTypeDef usart1_init_struct;
	/* Bit configuration structure for GPIOA PIN9 and PIN10 */
	GPIO_InitTypeDef gpioa_init_struct;
	 
	/* Enalbe clock for USART1, AFIO and GPIOA */
//    RCC_APB2PeriphClockCmd(RCC_APB2Periph_USART1 | //RCC_APB2Periph_AFIO | 
//                           RCC_APB2Periph_GPIOA, ENABLE);
	/* Enable the RCC,  USART1, GPIOA peripheral */
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_USART1, ENABLE);
	RCC_AHBPeriphClockCmd(RCC_AHBPeriph_GPIOA, ENABLE);
													
//    /* GPIOA PIN9 alternative function Tx */
//    gpioa_init_struct.GPIO_Pin = GPIO_Pin_9;
//    gpioa_init_struct.GPIO_Speed = GPIO_Speed_50MHz;
//    gpioa_init_struct.GPIO_Mode = GPIO_Mode_AF_PP;
//    GPIO_Init(GPIOA, &gpioa_init_struct);
//    /* GPIOA PIN10 alternative function Rx */
//    gpioa_init_struct.GPIO_Pin = GPIO_Pin_10;
//    gpioa_init_struct.GPIO_Speed = GPIO_Speed_50MHz;
//    gpioa_init_struct.GPIO_Mode = GPIO_Mode_IN_FLOATING;
//    GPIO_Init(GPIOA, &gpioa_init_struct);

	/* Initialize the RTS and TX */
	gpioa_init_struct.GPIO_Pin = GPIO_Pin_9;
	gpioa_init_struct.GPIO_Mode = GPIO_Mode_AF;
	gpioa_init_struct.GPIO_OType = GPIO_OType_PP;
	gpioa_init_struct.GPIO_PuPd = GPIO_PuPd_NOPULL;
	gpioa_init_struct.GPIO_Speed = GPIO_Speed_50MHz;
//	gpioa_init_struct.GPIO_Speed = GPIO_Speed_10MHz;
	GPIO_Init(GPIOA, &gpioa_init_struct);

	/* Initialize the RX */
	gpioa_init_struct.GPIO_Pin = GPIO_Pin_10;
	gpioa_init_struct.GPIO_Mode = GPIO_Mode_AF;
	gpioa_init_struct.GPIO_PuPd = GPIO_PuPd_NOPULL;
	gpioa_init_struct.GPIO_Speed = GPIO_Speed_50MHz;
	GPIO_Init(GPIOA, &gpioa_init_struct);

	/* Set alternate function 7 for USART pins */
	GPIO_PinAFConfig(GPIOA, GPIO_PinSource9, GPIO_AF_7);
	GPIO_PinAFConfig(GPIOA, GPIO_PinSource10, GPIO_AF_7);


	/* Baud rate, 8-bit data, One stop bit
	 * No parity, Do both Rx and Tx, No HW flow control
	 */
	usart1_init_struct.USART_BaudRate = USART_BaudRate_;   
	usart1_init_struct.USART_WordLength = USART_WordLength_8b;  
	usart1_init_struct.USART_StopBits = USART_StopBits_1;   
	usart1_init_struct.USART_Parity = USART_Parity_No ;
	usart1_init_struct.USART_Mode = USART_Mode_Rx | USART_Mode_Tx;
	usart1_init_struct.USART_HardwareFlowControl = USART_HardwareFlowControl_None;
	/* Configure USART1 */
	USART_Init(USART1, &usart1_init_struct);
	/* Enable RXNE interrupt */
	USART_ITConfig(USART1, USART_IT_RXNE, ENABLE);
	/* Enable USART1 */
	USART_Cmd(USART1, ENABLE);  
	/* Enable USART1 global interrupt */
	NVIC_EnableIRQ(USART1_IRQn);

//	/* Setting up USART */
//	/* Use 1 Stop Bit (no change to CR2 register) */
//	/* Set WordLength to 9b, Parity to Odd, enable rx and tx */
//	USART1->CR1 |= USART_CR1_M | USART_CR1_PCE | USART_CR1_PS |
//	    USART_CR1_TE | USART_CR1_RE;
//	USART1->CR3 |= USART_CR3_CTSE | USART_CR3_RTSE;

//	/* Configure the USART Baud Rate */
//	RCC_GetClocksFreq(&RCC_ClocksStatus);
//	apbclock = RCC_ClocksStatus.PCLK1_Frequency;	
//	br = apbclock / USART_BaudRate_;
//	USART1->BRR = ((br & 0xf) >> 1) | (br & ~0xf);

//	/* Enable the UART */
//	USART1->CR1 |= USART_CR1_UE;

//	/* Enable RXNE interrupt */
//	USART_ITConfig(USART1, USART_IT_RXNE, ENABLE);

//	/* Enable USART1 global interrupt */
//	NVIC_EnableIRQ(USART1_IRQn);

	stm32_set_led(GPIO_Pin_10, 1);
}
  
/**********************************************************
 * USART1 interrupt request handler: on reception of a 
 * character 't', toggle LED and transmit a character 'T'
 *********************************************************/
void USART1_IRQHandler(void)
{
	stm32_set_led(GPIO_Pin_11, 0);
    /* RXNE handler */
    if(USART_GetITStatus(USART1, USART_IT_RXNE) != RESET)
    {
        /* If received 't', toggle LED and transmit 'T' */
        if((char)USART_ReceiveData(USART1) == 't')
        {
            USART_SendData(USART1, 'T');
            /* Wait until Tx data register is empty, not really 
             * required for this example but put in here anyway.
             */
            /*
            while(USART_GetFlagStatus(USART1, USART_FLAG_TXE) == RESET)
            {
            }*/
        }
    }
     
    /* ------------------------------------------------------------ */
    /* Other USART1 interrupts handler can go here ...             */
}   


void USART1_SendBuff(u8 *buff, u16 len)
{
	u16 i = 0;
	for(i=0; i<len; i++)
	{
		USART_SendData(USART1, buff[i]);
		/* Wait until Tx data register is empty, not really 
		 * required for this example but put in here anyway.
		 */
		while(USART_GetFlagStatus(USART1, USART_FLAG_TXE) == RESET)
		{
		}
	}
}	
