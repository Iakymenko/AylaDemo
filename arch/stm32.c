/*
 * Copyright 2014 Ayla Networks, Inc.  All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 * The views and conclusions contained in the software and documentation are
 * those of the authors and should not be interpreted as representing official
 * policies, either expressed or implied, of Ayla Networks, Inc.
 */

#include <string.h>
#include <ayla/mcu_platform.h>
#include <mcu_io.h>
#include <stm32.h>
#ifdef DEMO_UART
#include <ayla/uart.h>
#else
#include <spi_platform_arch.h>
#endif /* DEMO_UART */
#include <ayla/cmp.h>
#include <demo.h>
#include <ayla/uart1.h>

#define FILLING_MAX_TIME 5 //in seconds

//controls cup filling bar
//j0.val=30$FF$FF$FF 

const u16 cup_filling_bar_len = 13;
u8 cup_filling_bar[cup_filling_bar_len] = {'j','0','.','v','a','l','=','0','0','0',0xFF,0xFF,0xFF };

static u8 turnwheel_fl = 0;
static u8 start_fill_fl = 0;
static u8 stop_fill_fl = 0;

struct stm32_debounce {
	u8	val;		/* exported value */
#include <stm32f30x.h>
	u8	raw;		/* last read value */
	u32	bounce_tick;	/* bouncing - ignore changes while this set */
	u32	off_tick;	/* on time extended while this is set */
};
static struct stm32_debounce stm32_button;
static struct stm32_debounce stm32_endswitch;

struct intr_stats intr_stats;
volatile u32 tick;
volatile u32 endsw_tick;

/*
 * Delay
 */
void stm32_delay_time(u32 ms)
{
	u32 end_tick = tick + (ms * SYSTICK_HZ) / 1000;

	while (cmp_gt(end_tick, tick)) {
		;
	}
}

/*
 * Reset the module
 */
void stm32_reset_module(void)
{
	int i;

	/*
	 * setup reset line to module, and pulse it.
	 */
	RESET_N_GPIO->BRR = bit(RESET_N_PIN);
	for (i = 1000; i--; )
		;
	RESET_N_GPIO->BSRR = bit(RESET_N_PIN);
}

#ifdef STM32F30X
static void stm32_intr_init(void)
{
	NVIC_InitTypeDef nvic_init;
	EXTI_InitTypeDef exti_init;
	extern void *__Vectors;

	/*
	 * Make sure vector table offset is set correctly before enabling
	 * interrupts.
	 */
	SCB->VTOR = (u32)&__Vectors;

	/* Enable SYSCFG clock */
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_SYSCFG, ENABLE);

	NVIC_PriorityGroupConfig(NVIC_PriorityGroup_4);	/* 4 bits priority */

#ifndef DEMO_UART
	spi_platform_intr_init();
#endif
	/*
	 * Set button to cause external interrupt on either edge.
	 */
	SYSCFG_EXTILineConfig(EXTI_PortSourceGPIOA, EXTI_PinSource0);
	exti_init.EXTI_Line = BUTTON_EXT_LINE;
	exti_init.EXTI_Mode = EXTI_Mode_Interrupt;
	exti_init.EXTI_Trigger = EXTI_Trigger_Rising_Falling;
	exti_init.EXTI_LineCmd = ENABLE;
	EXTI_Init(&exti_init);

	nvic_init.NVIC_IRQChannel = BUTTON_IRQ;
	nvic_init.NVIC_IRQChannelPreemptionPriority = 15;
	nvic_init.NVIC_IRQChannelSubPriority = 0;
	nvic_init.NVIC_IRQChannelCmd = ENABLE;
	NVIC_Init(&nvic_init);

	/*
	 * Set endswitch to cause external interrupt on either edge.
	 */
	 // TODO: Change EXTI_PinSource14 by some Macros
	SYSCFG_EXTILineConfig(EXTI_PortSourceGPIOA, EXTI_PinSource4);
	exti_init.EXTI_Line = ENDSWITCH_EXT_LINE;
	exti_init.EXTI_Mode = EXTI_Mode_Interrupt;
	exti_init.EXTI_Trigger = EXTI_Trigger_Rising_Falling;
	exti_init.EXTI_LineCmd = ENABLE;
	EXTI_Init(&exti_init);

	nvic_init.NVIC_IRQChannel = ENDSWITCH_IRQ;
	nvic_init.NVIC_IRQChannelPreemptionPriority = 15;
	nvic_init.NVIC_IRQChannelSubPriority = 0;
	nvic_init.NVIC_IRQChannelCmd = ENABLE;
	NVIC_Init(&nvic_init);

}
#else
static void stm32_intr_init(void)
{
	NVIC_InitTypeDef nvic_init;
	EXTI_InitTypeDef exti_init;

	RCC->APB2ENR |= RCC_APB2Periph_AFIO;

	NVIC_PriorityGroupConfig(NVIC_PriorityGroup_4);	/* 4 bits priority */

#ifndef DEMO_UART
	spi_platform_intr_init();
#endif
	/*
	 * Set button to cause external interrupt on either edge.
	 */
	GPIO_EXTILineConfig(BUTTON_PORT_SOURCE, BUTTON_PIN_SOURCE);
	exti_init.EXTI_Line = BUTTON_EXT_LINE;
	exti_init.EXTI_Mode = EXTI_Mode_Interrupt;
	exti_init.EXTI_Trigger = EXTI_Trigger_Rising_Falling;
	exti_init.EXTI_LineCmd = ENABLE;
	EXTI_Init(&exti_init);

	nvic_init.NVIC_IRQChannel = BUTTON_IRQ;
	nvic_init.NVIC_IRQChannelPreemptionPriority = 15;
	nvic_init.NVIC_IRQChannelSubPriority = 0;
	nvic_init.NVIC_IRQChannelCmd = ENABLE;
	NVIC_Init(&nvic_init);

	/*
	 * Set endswitch to cause external interrupt on either edge.
	 */
	GPIO_EXTILineConfig(ENDSWITCH_PORT_SOURCE, ENDSWITCH_PIN_SOURCE);
	exti_init.EXTI_Line = ENDSWITCH_EXT_LINE;
	exti_init.EXTI_Mode = EXTI_Mode_Interrupt;
	exti_init.EXTI_Trigger = EXTI_Trigger_Rising_Falling;
	exti_init.EXTI_LineCmd = ENABLE;
	EXTI_Init(&exti_init);

	nvic_init.NVIC_IRQChannel = ENDSWITCH_IRQ;
	nvic_init.NVIC_IRQChannelPreemptionPriority = 15;
	nvic_init.NVIC_IRQChannelSubPriority = 0;
	nvic_init.NVIC_IRQChannelCmd = ENABLE;
	NVIC_Init(&nvic_init);

}
#endif

/*
 * Check if module is ready
 */
int stm32_ready(void)
{
	return (READY_N_GPIO->IDR & bit(READY_N_PIN)) == 0;
}

/*
 * Get the state of a LED
 */
int stm32_get_led(u32 pin)
{
	return ((pin & LED_UGPIO->ODR) != 0);
}

/*
 * Set an LED to a value
 */
void stm32_set_led(u32 pin, u8 val)
{
	if (val) {
		LED_UGPIO->BSRR = pin;
	} else {
		LED_UGPIO->BRR = pin;
	}
}

/*
 * Set the LED used to indicate factory reset
 */
void stm32_set_factory_rst_led(int on)
{
	stm32_set_led(bit(LED0_UPIN), on);
}

/*
 * Get the state of a Pump
 */
int stm32_get_pump(u32 pin)
{
	return ((pin & PUMP_GPIO->ODR) != 0);
}

/*
 * Set an Pump to a value
 */
void stm32_set_pump(u32 pin, u8 val)
{
	if (val) {
		PUMP_GPIO->BSRR = pin;
	} else {
		PUMP_GPIO->BRR = pin;
	}
}

/*
 * Set an LED turning wheel carousel
 */
void stm32_turn_the_wheel(u8 val)
{
	if (val) {
		turnwheel_fl = 1;
	} else {
		turnwheel_fl = 0;
		stm32_set_led(GPIO_Pin_15, 0);
		stm32_set_led(GPIO_Pin_14, 0);
		stm32_set_led(GPIO_Pin_13, 0);
		stm32_set_led(GPIO_Pin_12, 0);
		stm32_set_led(GPIO_Pin_11, 0);
		stm32_set_led(GPIO_Pin_10, 0);
		stm32_set_led(GPIO_Pin_9, 0);
		stm32_set_led(GPIO_Pin_8, 0);
	}
}

/*
 * Set start or stop cup filling
 */
void stm32_startstop_filling(u8 val)
{
	if (val) {
		start_fill_fl = 1;
	} else {
		stop_fill_fl = 1;
	}
}

void startstop_routine(void)
{
	const u32 filling_max_ticks = SYSTICK_HZ*FILLING_MAX_TIME;
	static u32 filling_tick = 0;
		
	// TODO: check here if cup is empty
	if(start_fill_fl && !filling_tick){
		stm32_set_pump((1 << PUMP_PIN), 1);
		filling_tick ++;
		start_fill_fl = 0;
	}
	
	if(filling_tick){
		filling_tick++;
	}
	
	if(stop_fill_fl || filling_tick > filling_max_ticks){
		filling_tick = 0;
		start_fill_fl = 0;
		stop_fill_fl = 0;
		stm32_set_pump((1 << PUMP_PIN), 0);
	}
}

void led_tick_routine(void)
{
	const u32 tick_max = SYSTICK_HZ/10;
	static u8 curr_led_position = 0;
	static u32 led_tick_cnt = 0;
	static u16 cup_filling_bar_cnt = 0;
	
	if(turnwheel_fl) {
		if(led_tick_cnt >= tick_max) {
			// Shouting down all LEDs
			stm32_set_led(GPIO_Pin_15, 0);
			stm32_set_led(GPIO_Pin_14, 0);
			stm32_set_led(GPIO_Pin_13, 0);
			stm32_set_led(GPIO_Pin_12, 0);
			stm32_set_led(GPIO_Pin_11, 0);
			stm32_set_led(GPIO_Pin_10, 0);
			stm32_set_led(GPIO_Pin_9, 0);
			stm32_set_led(GPIO_Pin_8, 0);
			// Turning ON next LED
			switch(curr_led_position) {
				case 0: {
					stm32_set_led(GPIO_Pin_15, 1);
					break;
				}
				case 1: {
					stm32_set_led(GPIO_Pin_14, 1);
					break;
				}
				case 2: {
					stm32_set_led(GPIO_Pin_13, 1);
					break;
				}
				case 3: {
					stm32_set_led(GPIO_Pin_12, 1);
					break;
				}
				case 4: {
					stm32_set_led(GPIO_Pin_11, 1);
					break;
				}
				case 5: {
					stm32_set_led(GPIO_Pin_10, 1);
					break;
				}
				case 6: {
					stm32_set_led(GPIO_Pin_9, 1);
					break;
				}
				case 7: {
					stm32_set_led(GPIO_Pin_8, 1);
					break;
				}
				default:
					break;
			}
			if(curr_led_position >= 7) {
				curr_led_position = 0;

				if(	cup_filling_bar_cnt>100)
					cup_filling_bar_cnt = 0;
				else
					cup_filling_bar_cnt+=5;

				cup_filling_bar[7] = 0x30 + (cup_filling_bar_cnt/100)%10;
				cup_filling_bar[8] = 0x30 + (cup_filling_bar_cnt/10)%10;
				cup_filling_bar[9] = 0x30 + cup_filling_bar_cnt%10;
				USART1_SendBuff(cup_filling_bar, cup_filling_bar_len);
			} else {
				curr_led_position++;
			}
			led_tick_cnt = 0;
		} else {
			led_tick_cnt++;
		}
	} else {
		led_tick_cnt = 0;
		curr_led_position = 0;
	}
}


static void stm32_systick_init(void)
{
	SysTick_Config(CPU_CLK_HZ / SYSTICK_HZ);
}

/*
 * Read pushbutton and debounce it.
 * Extend the on time to make it at least BUTTON_ON_MIN_TICKS long.
 * Don't report changes within BUTTON_DEBOUNCE_TICKS of a previous change.
 * Return the debounced, not the extended value.
 */
static u8 stm32_button_read(void)
{
	u8 new;

	new = (BUTTON_GPIO->IDR & bit(BUTTON_PIN)) != 0;
	if (stm32_button.raw == new) {
		return new;
	}
	stm32_button.raw = new;
	if (stm32_button.bounce_tick) {
		stm32_button.bounce_tick++;
		new = stm32_button.val;
	} else {
		stm32_button.bounce_tick = tick + BUTTON_DEBOUNCE_TICKS;
		if (!stm32_button.off_tick) {
			if (new) {
				stm32_button.off_tick =
				     tick + BUTTON_ON_MIN_TICKS;
			}
			stm32_button.val = new;
			demo_set_button_state(stm32_button.val);
		}
	}
	return new;
}

/*
 * Read endswitch and debounce it.
 * Extend the on time to make it at least BUTTON_ON_MIN_TICKS long.
 * Don't report changes within BUTTON_DEBOUNCE_TICKS of a previous change.
 * Return the debounced, not the extended value.
 */
static u8 stm32_endswitch_read(void)
{
	u8 new;

	// if logic 0 then pressed
	new = (ENDSWITCH_GPIO->IDR & bit(ENDSWITCH_PIN)) == 0; 
	if (stm32_endswitch.raw == new) {
		return new;
	}
	stm32_endswitch.raw = new;
	if (stm32_endswitch.bounce_tick) {
		stm32_endswitch.bounce_tick++;
		new = stm32_endswitch.val;
	} else {
		stm32_endswitch.bounce_tick = endsw_tick + BUTTON_DEBOUNCE_TICKS;
		if (!stm32_endswitch.off_tick) {
			if (new) {
				stm32_endswitch.off_tick =
				     endsw_tick + BUTTON_ON_MIN_TICKS;
			}
			stm32_endswitch.val = new;
			set_endswitch_state(stm32_endswitch.val);
		}
	}
	return new;
}

/*
 * Button Interrupt Handler.
 */
#if !defined(DEMO_UART) || defined(STM32F30X)
void EXTI0_IRQHandler(void)
#else
void EXTI9_5_IRQHandler(void)
#endif
{
	intr_stats.button++;
	if (!EXTI_GetITStatus(BUTTON_EXT_LINE)) {
		return;
	}
	EXTI_ClearITPendingBit(BUTTON_EXT_LINE);
	stm32_button_read();
}

/*
 * Endswitch Interrupt Handler.
 */
#if !defined(DEMO_UART) || defined(STM32F30X)
void EXTI4_IRQHandler(void)
#else
// TODO: change below on something like: void EXTI9_5_IRQHandler(void)
void SomeHandler(void)
#endif
{
	intr_stats.endswitch++;
	if (!EXTI_GetITStatus(ENDSWITCH_EXT_LINE)) {
		return;
	}
	EXTI_ClearITPendingBit(ENDSWITCH_EXT_LINE);
	stm32_endswitch_read();
}

void SysTick_Handler(void)
{
	tick++;
	endsw_tick++;
	
	if (stm32_button.bounce_tick && cmp_gt(tick, stm32_button.bounce_tick)) {
		stm32_button.bounce_tick = 0;
		stm32_button_read();
	}
	if (stm32_button.off_tick && cmp_gt(tick, stm32_button.off_tick)) {
		stm32_button.off_tick = 0;
		stm32_button_read();
		if (stm32_button.val > stm32_button.raw) {
			stm32_button.val = stm32_button.raw;
			demo_set_button_state(stm32_button.val);
		}
	}
	// endswitch handling 
	// TODO: combine this with a button sys tick handling
	if (stm32_endswitch.bounce_tick && cmp_gt(endsw_tick, stm32_endswitch.bounce_tick)) {
		stm32_endswitch.bounce_tick = 0;
		stm32_endswitch_read();
	}
	if (stm32_endswitch.off_tick && cmp_gt(endsw_tick, stm32_endswitch.off_tick)) {
		stm32_endswitch.off_tick = 0;
		stm32_endswitch_read();
		if (stm32_endswitch.val > stm32_endswitch.raw) {
			stm32_endswitch.val = stm32_endswitch.raw;
			set_endswitch_state(stm32_endswitch.val);
		}
	}

	// LED routine
	led_tick_routine();
	// filling routine
	startstop_routine();
}

/*
 * See if user is holding down the blue button during boot.
 * The button must be down for 5 seconds total.  After 1 second we
 * start blinking the blue LED.  After 5 seconds we do the reset.
 */
int stm32_factory_reset_detect(void)
{
	int next_blink = 0;
	int reset_time;
	int led = 0;

	reset_time = tick + SYSTICK_HZ * 5;
	while (cmp_gt(reset_time, tick)) {
		if (!stm32_button_read()) {
			stm32_set_factory_rst_led(0);
			return 0;
		}
		if (cmp_gt(tick, next_blink)) {
			next_blink = tick + SYSTICK_HZ / 8; /* 8 Hz blink */
			led ^= 1;
			stm32_set_factory_rst_led(led);
		}
	}
	stm32_set_factory_rst_led(1);
	return 1;
}

/*
 * Initialize the stm32
 */
void stm32_init(void)
{
	stm32_systick_init();
	stm32_intr_init();
}
